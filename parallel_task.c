#include "wramp.h"

void parallel_main()
{
    // Variables must be declared at the top of a block
    int mode = 0;
    int buttons, switches, thousands, hundreds, tens, ones;

    // Infinite loop
    while (1)
    {
        // Read current value from parallel buttons register
        buttons = WrampParallel->Buttons;
        if (buttons == 1)
        {
            mode = 0;
        }
        else if (buttons == 2)
        {
            mode = 1;
        }
        else if (buttons == 4)
        {
            return;
        }

        // Read current value from parallel switch register
        switches = WrampParallel->Switches;

        if (mode != 0)
        {
            // Convert to decimal
            thousands = switches / 1000;
            switches -= thousands * 1000;
            hundreds = switches / 100;
            switches -= hundreds * 100;
            tens = switches / 10;
            switches -= tens * 10;
            ones = switches;
            switches -= ones;

            switches |= thousands << 12;
            switches |= hundreds << 8;
            switches |= tens << 4;
            switches |= ones;
        }

        WrampParallel->UpperLeftSSD = (switches & 0xF000) >> 12;
        WrampParallel->UpperRightSSD = (switches & 0x0F00) >> 8;
        WrampParallel->LowerLeftSSD = (switches & 0x00F0) >> 4;
        WrampParallel->LowerRightSSD = switches & 0x000F;
    }
}
