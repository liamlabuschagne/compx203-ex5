.equ timer_ctrl, 0x72000
.equ timer_load, 0x72001
.equ timer_int_ack, 0x72003
.global main
.text
main:
    # Create stack frame
    subui $sp,$sp,1 # Just storing $ra
    sw $ra,0($sp)

    # Setup interrupts
    
    # Configure CPU control register
    movsg $2, $cctrl # Copy the current value of $cctrl into $2
    andi $2, $2, 0x000f  # Mask (disable) all interrupts
    ori $2, $2, 0x42 # Enable IRQ2 and IE (global interrupt enable) 0100 0010 0x42
    movgs $cctrl, $2 # Copy the new CPU control value back to $cctrl

    # Set interrupt handler to be our own
    movsg $2, $evec # Copy the old handler’s address to $2
    sw $2, old_vector($0)  # Save it to memory
    la $2, handler # Get the address of our handler
    movgs $evec, $2 # And copy it into the $evec register

    # Enable auto restarting timer interrupt
    addui $2,$0,0x3 # Timer interrupt + Automatic restart enable
    sw $2,timer_ctrl($0)

    # Set timer to count down from 1/100 of a second
    addui $2,$0,0x18 # 0b00011000 Hex for 24 since the timer counts down at 2400Hz (2400 ticks per second)
    sw $2,timer_load($0)

    jal serial_main # Wait for user to press 'q' on SP2

    # Disable all the interrupts
    movsg $2, $cctrl # Copy the current value of $cctrl into $2
    andi $2, $2, 0x000f  # Mask (disable) all interrupts
    movgs $cctrl, $2 # Copy the new CPU control value back to $cctrl

    # Restore $evec to default handler
    lw $2, old_vector($0)  # Get the old exception vector
    movgs $evec, $2 # And copy it back into the $evec register

    # Restore $ra
    lw $ra,0($sp)

    # Destroy stack frame
    addui $sp,$sp,1

    jr $ra

handler:
    movsg $13, $estat # Get the value of the exception status register

    # Check if there are interrupts other than IRQ2
    andi $13, $13, 0xffb0 # All bits except bit 6 should be 0: 1011 0xb
    beqz $13, handle_irq2 # If so, go to our handler
    
    lw $13, old_vector($0) # Otherwise, jump to the default handler
    jr $13

handle_irq2:
    # Acknowledge the interrupt
    sw $0,timer_int_ack($0)

    # Increment counter
    lw $13,counter($0)
    addui $13,$13,1
    sw $13,counter($0)

    rfe # Return from the interrupt (rfe)

.bss
old_vector: .word