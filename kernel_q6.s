.equ timer_ctrl, 0x72000
.equ timer_load, 0x72001
.equ timer_int_ack, 0x72003

# Format for PCB
.equ pcb_link, 0
.equ pcb_reg1, 1
.equ pcb_reg2, 2
.equ pcb_reg3, 3
.equ pcb_reg4, 4
.equ pcb_reg5, 5
.equ pcb_reg6, 6
.equ pcb_reg7, 7
.equ pcb_reg8, 8
.equ pcb_reg9, 9
.equ pcb_reg10, 10
.equ pcb_reg11, 11
.equ pcb_reg12, 12
.equ pcb_reg13, 13
.equ pcb_sp, 14
.equ pcb_ra, 15
.equ pcb_ear, 16
.equ pcb_cctrl, 17

.global main
.text
main:
    # ----- Setup tasks

    # Setup for task 1
    # Unmask IRQ2,KU=1,OKU=1,IE=0,OIE=1
    addi $5, $0, 0x4d
    # Setup the pcb for task 1
    la $1, task1_pcb
    # Setup the link field
    la $2, task2_pcb # Next task is task 2
    sw $2, pcb_link($1)
    # Setup the stack pointer
    la $2, task1_stack
    sw $2, pcb_sp($1)
    # Setup the $ear field
    la $2, serial_main # Our first task is serial_task
    sw $2, pcb_ear($1)
    # Setup the $cctrl field
    sw $5, pcb_cctrl($1)

    # Setup for task 2
    # Unmask IRQ2,KU=1,OKU=1,IE=0,OIE=1
    addi $5, $0, 0x4d
    # Setup the pcb for task 2
    la $1, task2_pcb
    # Setup the link field
    la $2, task3_pcb # Next task is task 3
    sw $2, pcb_link($1)
    # Setup the stack pointer
    la $2, task2_stack
    sw $2, pcb_sp($1)
    # Setup the $ear field
    la $2, parallel_main # Our second task is parallel_task
    sw $2, pcb_ear($1)
    # Setup the $cctrl field
    sw $5, pcb_cctrl($1)

    # Setup for task 3
    # Unmask IRQ2,KU=1,OKU=1,IE=0,OIE=1
    addi $5, $0, 0x4d
    # Setup the pcb for task 3
    la $1, task3_pcb
    # Setup the link field
    la $2, task1_pcb # Next task is task 1
    sw $2, pcb_link($1)
    # Setup the stack pointer
    la $2, task3_stack
    sw $2, pcb_sp($1)
    # Setup the $ear field
    la $2, gameSelect_main # Our third task is gameSelect
    sw $2, pcb_ear($1)
    # Setup the $cctrl field
    sw $5, pcb_cctrl($1)

    # Set first task as the current task
    la $1, task1_pcb
    sw $1, current_task($0)

    # ----- Setup interrupts

    # Set interrupt handler to be our own
    movsg $2, $evec # Copy the old handler’s address to $2
    sw $2, old_vector($0)  # Save it to memory
    la $2, handler # Get the address of our handler
    movgs $evec, $2 # And copy it into the $evec register

    # Enable auto restarting timer interrupt
    addui $2,$0,0x3 # Timer interrupt + Automatic restart enable
    sw $2,timer_ctrl($0)

    # Set timer to count down from 1/100 of a second
    addui $2,$0,0x18 # 0b00011000 Hex for 24 since the timer counts down at 2400Hz (2400 ticks per second)
    sw $2,timer_load($0)

    # ---- Start first task (via dispatcher)
    j load_context

handler:
    # Check which exception occurred.
    movsg $13, $estat # Get the value of the exception status register

    # If it is the timer interrupt, jump to handle_timer
    andi $13, $13, 0xffb0 # All bits except bit 6 should be 0: 1011 0xb
    beqz $13, handle_timer # If so, go to our handler

    # Otherwise we jump to the default exception handler
    lw $13, old_vector($0)
    jr $13

handle_timer:

    # Acknowledge the interrupt
    sw $0,timer_int_ack($0)

    # Increment counter
    lw $13,counter($0)
    addui $13,$13,1
    sw $13,counter($0)

    # Subtract 1 from the timeslice counter
    lw $13, timeslice_counter($0)
    subui $13,$13,1
    sw $13, timeslice_counter($0)
    # If timeslice counter is zero, go to dispatcher
    beqz $13,dispatcher
    # Otherwise we return from exception.
    rfe

dispatcher:
    # Save context for current task
    save_context:
        # Get the base address of the current PCB
        lw $13, current_task($0)
        # Save the registers
        sw $1, pcb_reg1($13)
        sw $2, pcb_reg2($13)
        sw $3, pcb_reg3($13)
        sw $4, pcb_reg4($13)
        sw $5, pcb_reg5($13)
        sw $6, pcb_reg6($13)
        sw $7, pcb_reg7($13)
        sw $8, pcb_reg8($13)
        sw $9, pcb_reg9($13)
        sw $10, pcb_reg10($13)
        sw $11, pcb_reg11($13)
        sw $12, pcb_reg12($13)
        
        # $1 is saved now so we can use it
        # Get the old value of $13
        movsg $1, $ers
        # and save it to the pcb
        sw $1, pcb_reg13($13)
        
        # Save $ear
        movsg $1, $ear
        sw $1, pcb_ear($13)
        # Save $cctrl
        movsg $1, $cctrl
        sw $1, pcb_cctrl($13)

        # Save $sp
        sw $sp, pcb_sp($13)

    # Select (schedule) the next task
    schedule:
        lw $13, current_task($0) # Get current task
        lw $13, pcb_link($13) # Get next task from pcb_link field
        sw $13, current_task($0) # Set next task as current task
    # Load context for next task
    load_context:
        lw $13, current_task($0) # Get PCB of current task

        # Reset the timeslice counter to an appropriate value
        addui $1,$0,2
        sw $1,timeslice_counter($0)
        
        # Get the PCB value for $13 back into $ers
        lw $1, pcb_reg13($13)
        movgs $ers, $1
        # Restore $ear
        lw $1, pcb_ear($13)
        movgs $ear, $1
        # Restore $cctrl
        lw $1, pcb_cctrl($13)
        movgs $cctrl, $1

        # Restore $sp   
        lw $sp, pcb_sp($13)

        # Restore the other registers
        lw $1, pcb_reg1($13)
        lw $2, pcb_reg2($13)
        lw $3, pcb_reg3($13)
        lw $4, pcb_reg4($13)
        lw $5, pcb_reg5($13)
        lw $6, pcb_reg6($13)
        lw $7, pcb_reg7($13)
        lw $8, pcb_reg8($13)
        lw $9, pcb_reg9($13)
        lw $10, pcb_reg10($13)
        lw $11, pcb_reg11($13)
        lw $12, pcb_reg12($13)
        
        # Return to the new task
        rfe

.bss
timeslice_counter: .word
old_vector: .word
current_task: .word
task1_pcb:
    .space 18
# Stack for task 1
.space 200
task1_stack:

task2_pcb:
    .space 18
# Stack for task 2
.space 200
task2_stack:

task3_pcb:
    .space 18
# Stack for task 3
.space 200
task3_stack: