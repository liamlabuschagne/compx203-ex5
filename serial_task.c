#include "wramp.h"

int counter = 0; // 12 minutes and 34 seconds
void serial_main()
{
    char *string = "\rssss.ss\0"; // Create a string large enough to display any of the formats
    int mode = 1;

    while (1)
    {

        // Check if sp2 has incoming character
        if (WrampSp2->Stat & 1)
        {
            if (WrampSp2->Rx == '1')
            {
                mode = 1;
            }
            else if (WrampSp2->Rx == '2')
            {
                mode = 2;
            }
            else if (WrampSp2->Rx == '3')
            {
                mode = 3;
            }
            else if (WrampSp2->Rx == 'q')
            {
                return;
            }
        }

        if (mode == 1)
        {
            // Convert to mm:ss format
            // Separate minutes and seconds
            int seconds = counter / 100;
            int minutes = seconds / 60;
            seconds -= minutes * 60; // Remove whole minutes from seconds total

            string[1] = (minutes / 10) + '0';
            string[2] = (minutes % 10) + '0';
            string[3] = ':';
            string[4] = (seconds / 10) + '0';
            string[5] = (seconds % 10) + '0';
            string[6] = ' ';
            string[7] = ' ';
        }
        else if (mode == 2)
        {
            // Convert to \rssss.ss\0 format
            int i = 7;
            int counterCopy = counter;
            while (i)
            {
                string[i] = (counterCopy % 10) + '0';
                counterCopy /= 10;
                i--;
                if (i == 5)
                {
                    string[i] = '.';
                    i--;
                }
            }
        }
        else if (mode == 3)
        {
            // Convert to \rtttttt\0 format
            int i = 6;
            int counterCopy = counter;
            while (i)
            {
                string[i] = (counterCopy % 10) + '0';
                counterCopy /= 10;
                i--;
            }
            string[7] = ' ';
        }

        {
            int i = 0;
            while (string[i] != 0)
            {
                // Check if serial port is ready to transmit
                if (WrampSp2->Stat & 2)
                {
                    WrampSp2->Tx = string[i];
                    i++;
                }
            }
        }
    }
}
