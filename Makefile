all:
	make switches &
	make parallel_task &
	make serial_task &

serial_task:
	wcc -o serial_task.srec serial_entry.c serial_task.c

parallel_task:
	wcc -o parallel_task.srec parallel_entry.c parallel_task.c

switches:
	wcc -o switches.srec switches.c
	
kernel_q3:
	wcc -c serial_task.c
	wasm kernel_q3.s
	wlink -o kernel_q3.srec serial_task.o kernel_q3.o

kernel_q4:
	wcc -c serial_task.c
	wasm kernel_q4.s
	wlink -o kernel_q4.srec serial_task.o kernel_q4.o

kernel_q5:
	wcc -c serial_task.c parallel_task.c
	wasm kernel_q5.s
	wlink -o kernel_q5.srec serial_task.o parallel_task.o kernel_q5.o

kernel_q6:
	wcc -c serial_task.c parallel_task.c
	wasm kernel_q6.s
	wlink -o kernel_q6.srec serial_task.o parallel_task.o kernel_q6.o /home/compx203/ex5-kernel/breakout.O /home/compx203/ex5-kernel/rocks.O /home/compx203/ex5-kernel/gameSelect.O

kernel_q7:
	wcc -c serial_task.c parallel_task.c
	wasm kernel_q7.s
	wlink -o kernel_q7.srec serial_task.o parallel_task.o kernel_q7.o /home/compx203/ex5-kernel/breakout.O /home/compx203/ex5-kernel/rocks.O /home/compx203/ex5-kernel/gameSelect.O

kernel_q8:
	wcc -c serial_task.c parallel_task.c
	wasm kernel_q8.s
	wlink -o kernel_q8.srec serial_task.o parallel_task.o kernel_q8.o /home/compx203/ex5-kernel/breakout.O /home/compx203/ex5-kernel/rocks.O /home/compx203/ex5-kernel/gameSelect.O

kernel_q9:
	wcc -c serial_task.c parallel_task.c
	wasm kernel_q9.s
	wlink -o kernel_q9.srec serial_task.o parallel_task.o kernel_q9.o /home/compx203/ex5-kernel/breakout.O /home/compx203/ex5-kernel/rocks.O /home/compx203/ex5-kernel/gameSelect.O


clean:
	rm *.srec *.o
	